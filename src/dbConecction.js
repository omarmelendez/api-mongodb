const mongoose = require('mongoose');

const conecctionDB = async() => {
    try {
        const DB = await mongoose.connect('mongodb://localhost:27017/impulsa');
        console.log("conexión exitosa: " + DB.connection.name);
    } catch (error) {
        console.log(error);
    }
}

module.exports = conecctionDB;