const { Schema, model } = require("mongoose");

const UserSchema = new Schema({
    firstname: String,
    lastname: String,
    birthday: Date,
    phone: String,
    password: {
        type: String,
        required: true,
    },
});

module.exports = model("User", UserSchema);