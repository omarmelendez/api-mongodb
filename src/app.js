const express = require('express');
const routerUsers = require('./routes/usersRoutes');
const conecctionDB = require('./dbConecction');
const app = express();

conecctionDB();

//settings
app.set("name", "API MongoDB");
app.set("port", process.env.port || 8000);

app.use(express.json())

app.get('/', (req, res) => {
    res.send('Hello World!')
})

app.use('/api/users', routerUsers);


module.exports = app;