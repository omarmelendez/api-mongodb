const { Router } = require('express');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
// app.use(express.urlencoded());
app.use(bodyParser.urlencoded());
app.use(express.json());
// const app = require('../app');
const usersController = require('../controllers/usersControllers');
const routerUsers = Router();



//GET
routerUsers.get('/', usersController.get);

//POST
routerUsers.post('/', usersController.add);
routerUsers.post('/login', usersController.login);

//PUT
routerUsers.put('/:id', usersController.update);

//DELETE
routerUsers.delete('/:id', usersController.delete);

module.exports = routerUsers;