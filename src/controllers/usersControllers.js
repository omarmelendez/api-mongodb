const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const config = require('./../../config/config');
const user = require('../models/usersModel');

// app.use(express.urlencoded());
app.use(bodyParser.urlencoded());
app.use(express.json());

app.set('key', config.key);

exports.login = async(req, res) => {
    const { firstname, password } = req.body;
    try {
        const users = await user.findOne({ firstname: firstname });
        // res.json(users);
        console.log(users)

        if (users != null) {
            console.log('login')
            if (users.password == password) {
                console.log("otra vez Login!!!!!!!!")
                jwt.sign({ users: users }, 'secretkey', (err, token) => {
                    res.json({
                        message: 'Usuarios y contraseña correctos',
                        token
                    })
                })
            } else {
                res.json({ msj: 'Contraseña invalida' })
                console.log("La contraseña es: " + validPassword)
            }

        } else {
            console.log("No encontrado")
            res.json({ msj: 'Usuario No encontrado' })
        }
    } catch (error) {
        res.json(error);
    }
};

exports.get = async(req, res) => {
    try {
        const users = await user.find();
        console.log('viendo datos');
        res.json(users);
    } catch (error) {
        res.json(error);
    }
};

exports.add = async(req, res) => {
    console.log("😎 Body: ");
    console.log(typeof req.body);
    try {
        let { firstname, lastname, birthday, phone, password } = req.body;
        console.log(typeof lastname);
        if (firstname && lastname && birthday && phone && password) {
            console.log(firstname + lastname + birthday + phone + password + '🦘');
            const newUser = new user({
                firstname,
                lastname,
                birthday,
                phone,
                password,
            });
            console.log('agregando usuario 👌');
            await newUser.save();
            res.json('Usuario Creado');
        } else {
            res.json({ isOk: false, msj: 'Datos Requeridos' });
        }

    } catch (error) {
        res.json(error);
    }
};

exports.update = async(req, res) => {
    try {
        const id = req.params.id;
        const data = req.body;
        if (id && data) {
            await user.findByIdAndUpdate(id, data);
        }
        res.json('Datos Actualizados');
    } catch (error) {
        res.json(error);
    }
};

exports.delete = async(req, res) => {
    try {
        const id = req.params.id;
        console.log(id);
        const deleted = await user.findByIdAndDelete(id);
        res.json("Usuario elimando Correctamente");
    } catch (error) {
        res.json(error);
    }
};